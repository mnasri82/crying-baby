# Crying Baby

Développer un jeu où il faut intéragir avec un bébé qui pleure
Style animal virtuel (tamagotchi)

## Consignes

- le bébé peut être dans deux états, soit il est content (et il n'y a rien à faire), soit il pleure et il faut intervenir (en fonction de pourquoi il pleure).

- le bébé pleure pour 3 raisons: il faut le changer, il faut le nourire, ou alors il pleure pour rien (c'est un bébé)...

- il faut prévoir plusieurs boutons pour intéragir avec le bébé: donner à manger, changer et jouer avec (s'il pleure pour rien).

- Par défaut, le bébé est dans son état "content", au bout de 10 secondes, un état doit etre choisi au hasard, soit il pleure (pour une raison ou pour une autre), soit il reste content

- S'il est dans un état de pleur, il ne changera pas d'état avant qu'il y ai eu intervention (la bonne intervention)

- Si jamais il est laissé dans son état de pleure pendant plus d'une minute, alors c'est perdu (les services sociaux emportent le bébé #oliverTwist)

- Dès qu'il change d'état, un son doit retentire (s'il se met à pleurer, un son de pleur, s'il est content, un son de joie)

## Resources

Les resources sont fournis, les sons et vignettes pour représenter l'état deu bébé.

Les images pour informer de la raison pour laquelle de bébé pleure

Les fichiers finaux à utiliser sont dans le dossier ./src/assets/
